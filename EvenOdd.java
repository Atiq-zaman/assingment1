package DemoProject1;

import java.util.Scanner;
public class EvenOdd {

    public static void main(String[] args) {
        Scanner input= new Scanner (System.in);
        System.out.print("Enter the Starting number: ");
        int start = input.nextInt();
        System.out.print("Enter the Ending number: ");
        int end = input.nextInt();
        int oddCount = 0;
        int evenCount = 0;
        if(start>0 && end>0) {

            for (int i = start; i <= end; i++) {
                if (i % 2 == 0) {
                    evenCount++;
                } else if (i % 2 == 1) {
                    oddCount++;
                }
            }
            System.out.println("Total event number count:" + oddCount);
            System.out.println("Total odd number count:" + evenCount);

            System.out.print("The even numbers between "+start+" and "+end+" are: ");

            for(int i=start;i<=end;i++){
                if(i%2==0) {
                    System.out.print(i+ " ");
                }
            }
        }
    }

}